package com.example.kalkulator_2_el;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_delete, btn_kurang, btn_tambah, btn_kali, btn_bagi, btn_koma, btn_sama_dengan, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;
    EditText R_hasil;
    float var1, var2;
    boolean b_kali, b_kurang, b_tambah, b_bagi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        R_hasil = (EditText) findViewById(R.id.hsl);
        btn_delete = (Button) findViewById(R.id.hps);
        btn_delete.setOnClickListener(this);
        btn1 = (Button) findViewById(R.id.angk1);
        btn1.setOnClickListener(this);
        btn2 = (Button) findViewById(R.id.angk2);
        btn2.setOnClickListener(this);
        btn3 = (Button) findViewById(R.id.angk3);
        btn3.setOnClickListener(this);
        btn4 = (Button) findViewById(R.id.angk4);
        btn4.setOnClickListener(this);
        btn5 = (Button) findViewById(R.id.angk5);
        btn5.setOnClickListener(this);
        btn6 = (Button) findViewById(R.id.angk6);
        btn6.setOnClickListener(this);
        btn7 = (Button) findViewById(R.id.angk7);
        btn7.setOnClickListener(this);
        btn8 = (Button) findViewById(R.id.angk8);
        btn8.setOnClickListener(this);
        btn9 = (Button) findViewById(R.id.angk9);
        btn9.setOnClickListener(this);
        btn_kali = (Button) findViewById(R.id.x);
        btn_kali.setOnClickListener(this);
        btn_bagi = (Button) findViewById(R.id.bagi);
        btn_bagi.setOnClickListener(this);
        btn_kurang = (Button) findViewById(R.id.kurang);
        btn_kurang.setOnClickListener(this);
        btn_tambah = (Button) findViewById(R.id.jmlh);
        btn_tambah.setOnClickListener(this);
        btn_koma = (Button) findViewById(R.id.koma);
        btn_koma.setOnClickListener(this);
        btn_sama_dengan = (Button) findViewById(R.id.sama_dengan);
        btn_sama_dengan.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.angk1:
                R_hasil.setText(R_hasil.getText().toString().trim() + "1");
                break;
            case R.id.angk2:
                R_hasil.setText(R_hasil.getText().toString().trim() + "2");
                break;
            case R.id.angk3:
                R_hasil.setText(R_hasil.getText().toString().trim() + "3");
                break;
            case R.id.angk4:
                R_hasil.setText(R_hasil.getText().toString().trim() + "4");
                break;
            case R.id.angk5:
                R_hasil.setText(R_hasil.getText().toString().trim() + "5");
                break;
            case R.id.angk6:
                R_hasil.setText(R_hasil.getText().toString().trim() + "6");
                break;
            case R.id.angk7:
                R_hasil.setText(R_hasil.getText().toString().trim() + "7");
                break;
            case R.id.angk8:
                R_hasil.setText(R_hasil.getText().toString().trim() + "8");
                break;
            case R.id.angk9:
                R_hasil.setText(R_hasil.getText().toString().trim() + "9");
                break;
            case R.id.angk0:
                R_hasil.setText(R_hasil.getText().toString().trim() + "0");
                break;
            case R.id.hps:
                R_hasil.setText("");
                break;
            case R.id.jmlh:
                var1 = Float.parseFloat(R_hasil.getText().toString().trim());
                b_tambah = true;
                R_hasil.setText(null);
                break;
            case R.id.kurang:
                var1 = Float.parseFloat(R_hasil.getText().toString().trim());
                b_kurang = true;
                R_hasil.setText(null);
                break;
            case R.id.bagi:
                var1 = Float.parseFloat(R_hasil.getText().toString().trim());
                b_bagi = true;
                R_hasil.setText(null);
                break;
            case R.id.x:
                var1 = Float.parseFloat(R_hasil.getText().toString().trim());
                b_kali = true;
                R_hasil.setText(null);
                break;
            case R.id.sama_dengan:
                var2 = Float.parseFloat(R_hasil.getText().toString().trim());
                if (b_tambah == true) {
                    R_hasil.setText(var1 + var2 + "");
                    b_tambah = false;
                } else if (b_kurang == true) {
                    R_hasil.setText(var1 - var2 + "");
                    b_kurang = false;
                } else if (b_kali == true) {
                    R_hasil.setText(var1 * var2 + "");
                    b_kali = false;
                } else if (b_bagi == true) {
                    R_hasil.setText(var1 / var2 + "");
                    b_bagi = false;
                }
        }
    }
}
